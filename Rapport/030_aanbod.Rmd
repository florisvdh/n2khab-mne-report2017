# Het bestaande aanbod van milieumonitoring voor het natuur- en milieubeleid {#aanbod}

```{block2, type = "alertblock", latex.options = "{Om zeker te onthouden}", echo = !keuze_html}

Kopieer uit HTML-versie!


```

```{block2, type = "alertblock", echo = keuze_html}
**Om zeker te onthouden**

- Er lopen binnen de Vlaamse overheid diverse initiatieven van milieumonitoring met toepassing op niveau Vlaanderen. De vaakst voorkomende trekker is de VMM, gevolgd door INBO. De meeste initiatieven staan op zichzelf en dienen een eigen specifiek doel.
- Een aandachtspunt is dat de MNM mogelijks op deze initiatieven kunnen voortbouwen en daarnaast een toegevoegde waarde kunnen hebben voor deze initiatieven.
- Een vergelijking van de aanpak door enkele andere lidstaten is te vinden in bijlage \@ref(app:buitenland). Een verder gevorderd voorbeeld is Denemarken, waar voor terrestrische habitats diverse metingen van water en bodem zijn geïntegreerd in de monitoring voor de artikel-17-rapportage.

```


Er bestaan momenteel reeds verschillende meet- en modelinitiatieven door de Vlaamse Overheid in verband met milieu. Op deze initiatieven kan mogelijks worden aangesloten met de MNM, indien ze al een gedeeltelijke invulling bieden van de vraagstelling in deel \@ref(part-vraagstelling-van-het-natuurbeleid-voor-de-meetnetten-natuurlijk-milieu).  Dit 'bestaande aanbod' vertoont nog lacunes ten opzichte van de vragen waar met de MNM een antwoord op moet worden gegeven. Er is echter ook de uitdaging in om het aanbod in de mate van het verantwoordbare te benutten voor het antwoord op die vragen. Dit aspect komt nader aan bod in deel \@ref(part-het-natuurbeleid-in-de-milieubeleidscontext).

Er is dus **reeds een traject afgelegd door de Vlaamse Overheid**, waarbij de MNM toegevoegde waarde hebben of waar de MNM op kunnen voortbouwen. Dat we bijgevolg niet starten vanaf nul, is een belangrijk aandachtspunt voor de MNM. 

Een overzicht van de voornaamste bestaande overheidsinitiatieven omtrent het systematisch verzamelen van milieu-informatie, met potentiële relevantie voor de MNM, is opgenomen in Tabel \@ref(tab:aanbod). Het gaat meer bepaald om deze die hun toepassing vinden in het beleid op niveau Vlaanderen. Meer overzichtsinformatie over deze initiatieven is te vinden in bijlage \@ref(app:milieumeetnetten).

Tijdens de kwalitatieve aanbodanalyse werden voor verschillende van deze initiatieven systematisch de kenmerken geïnventariseerd (volgens de lijst in bijlage \@ref(app:eigenschappen)). Deze tabellen -- met heel wat meer informatie en bijhorende achtergronddocumenten -- zijn niet in dit rapport opgenomen, maar worden nader behandeld in de meetnetspecifieke rapporten.

Enkele conclusies kunnen worden gemaakt bij Tabel \@ref(tab:aanbod):

- voor alle voor de MNM beschouwde milieucompartimenten bestaan er reeds initiatieven;
- er zijn meer initiatieven te vinden in het compartiment oppervlaktewater;
- de Vlaamse Milieumaatschappij (VMM) is de vaakst voorkomende trekker, gevolgd door INBO;
- er is relatief weinig overkoepelende rationale: de meeste initiatieven staan op zichzelf en dienen een eigen specifiek doel (al dan niet met een specifiek wettelijk kader);
- er is wel meer overlap in de wettelijke kaders, waarbij het Natuurdecreet, het Decreet Integraal Waterbeleid, VLAREM en het Mestdecreet het vaakst terugkeren; 
- het geografisch gebied waarbinnen de hier opgesomde initiatieven opereren, is meestal geheel Vlaanderen, maar het ruimtelijke niveau waarop en de eenheden waarover uitspraken worden gedaan, lopen sterk uiteen (van heel lokaal tot gewestelijk);
- voor sommige initiatieven worden modellen ingezet; dit komt voor binnen de compartimenten lucht (concentratie- en depositieberekeningen) en oppervlaktewater (peil- en kwaliteitsberekeningen);
- lucht- en grondwatermetingen gaan gepaard met vaste meetinstallaties op terrein, terwijl dit gedeeltelijk niet zo is voor oppervlaktewater en bodem^[In het compartiment oppervlaktewater worden wel vaste meetinstallaties gebruikt voor kwantiteit (peilen, debieten) en sedimenttransport. In het compartiment bodem kunnen vaste installaties worden gebruikt voor de bemeting van de bodemoplossing, bodemtemperatuur en bodemvocht.];
- de rapporteringsfrequenties zijn uiteenlopend tussen de initiatieven, en staan meestal in functie van overeenkomstige wettelijke vereisten in het milieu- of natuurbeleid.

\blandscape

\scriptsize

```{r TabelAanbod, results='asis', echo=FALSE, message=FALSE}
tabel1 <- read.xlsx2("../Tabellen/Bijlage MilieumeetnettenVlaamseOverheid.xlsx",sheetIndex=1,check.names = FALSE, colIndex = 1:8)
pandoc.table(
  tabel1,
  caption = "(\\#tab:aanbod)_Overzicht van de voornaamste bestaande overheidsinitiatieven rond het systematisch verzamelen van milieu-informatie, met oog op toepassing in het Vlaamse beleid._",
  split.table = Inf,
  split.cells = 20,
  justify = c(rep("left",2),rep("center",4),rep("left",2))
)

```

\normalsize

\elandscape

Ter informatie wordt in bijlage \@ref(app:buitenland) met voorbeelden aangegeven in welke mate **andere lidstaten** van de Europese Unie het natuurlijk milieu monitoren omwille van het natuurbeleid. De benaderingen lopen sterk uiteen, en hangen af van de mate waarin milieudrukken de doelstellingen van het natuurbeleid hypothekeren. Waar milieudrukken het natuurbeleid bemoeilijken, blijkt er een grotere neiging om het natuurlijk milieu te monitoren. Zo had Denemarken in 2005 een monitoringsprogramma klaar voor het milieu- en natuurbeleid, waarbij voor terrestrische habitats diverse metingen van water en bodem zijn geïntegreerd in de monitoring (meer info in bijlage \@ref(app:buitenland)). Tevens is er een effect te zien van hoe lang een land reeds lid is van de EU: jongere lidstaten zijn vaak nog bezig met het ontwerp van de monitoring.



