# Eindgebruik van de resultaten en noden aan rapportage {#gebruik}

```{block2, type = "alertblock", latex.options = "{Om zeker te onthouden}", echo = !keuze_html}

Kopieer uit HTML-versie!


```

```{block2, type = "alertblock", echo = keuze_html}
**Om zeker te onthouden**

- Er is een breed scala aan potentiële eindgebruikers van de MNM. Niet elke eindgebruiker is gebaat met dezelfde informatie of rapportagevorm. Zonder reeds keuzes te maken, wordt in dit hoofdstuk een rangordening opgesteld volgens belang van de eindgebruiker. Daarbij wordt aangegeven wat voor elke eindgebruiker een optimale vorm en frequentie van rapporteren zou zijn.
- Het Vlaamse natuurbeleid, het departement en de minister waaronder het ressorteert en de middenveldsectoren hebben de *hoogste* prioriteit. Voor deze eindgebruikers en voor de Europese Commissie is een *zesjaarlijkse* rapportage meest aangewezen.
- Voor ANB is het belangrijk dat de volgende informatie vlot uit rapporteringen te halen is:
    - beleidsprioritering van aan te pakken **milieuknelpunten** per habitattype of regionaal belangrijk biotoop, met daarbij de omvang van de problemen (diagnose op basis van toestand en trend);
    - aanduiding of het milieugerichte natuurbeleid **voor of achter zit op schema**, en hoeveel (voortgangsbewaking op basis van beoordeling).
- Behalve rapportage is ook een uitwerking van een professionele, doelgroepgerichte *communicatie* belangrijk, zowel over de implementatie van een meetnet als over de rapportagefasen.

```

Om in een later ontwerpstadium te kiezen welke concrete voorstelling van de antwoorden op de vragen nodig is, is het essentieel om een overzicht te hebben van de gebruiksfunctie van de Meetnetten Natuurlijk Milieu. In de hoofdstukken \@ref(verantwoording) en 
\@ref(vragen) kwam dit reeds uitgebreid aan bod voor het Vlaams en Europees natuurbeleid als meer abstracte 'eindgebruiker'. Om dit gebruik te kunnen vertalen naar de *gewenste vorm en frequentie van rapportering*, dient er een nader onderscheid te worden gemaakt volgens effectieve eindgebruikers van het natuurbeleid.

Daarnaast mag in dit verband niet vergeten worden dat *niet enkel het natuurbeleid zelf* eindgebruikers omvat. Hoewel de MNM worden ontworpen volgens *hun* belangrijkste milieu-informatiebehoefte, is er een ruimere interesse, buiten het natuurbeleid, om de resultaten van de MNM te kennen en te gebruiken. Zo zijn bijvoorbeeld verschillende middenveldsectoren zeer geïnteresseerd in de onderbouwing van het milieugerichte natuurbeleid.^[Deze sectoren
vertegenwoordigen de driving forces of de gemeenschapsrespons van de DPSIR-keten (paragraaf \@ref(dpsir)).]
Die onderbouwing bekomen via de MNM was onder meer dáárom een belangrijke reden voor het natuurbeleid om de MNM te ontwerpen; zie paragraaf \@ref(beleidscyclus).

Aangezien er een breed scala aan eindgebruikers bestaat, elk met hun eigen invalshoek, wordt in deze paragraaf een overzicht gegeven van hoe elke eindgebruiker (naar verwachting):

- de informatie uit de MNM vooral zal gebruiken;
- waarom dat zo is;
- met welke kernvragen dit correspondeert;
- welke frequentie en vorm(en) van resultaten daar best bij aansluiten.

In Tabel \@ref(tab:eindgebruik) wordt hiervan een overzicht gegeven. Het betreft een inschatting op basis van de relatie die verschillende eindgebruikers hebben ten aanzien van milieugericht natuurbeleid. Aangezien het niet noodzakelijk de bedoeling is om de wensen van alle eindgebruikers in te lossen (dat is niet noodzakelijk haalbaar), is er een prioritering aangebracht tussen eindgebruikers (prioriteit 1 = hoogste prioriteit).

Deze tabel is ook bijgevoegd als xlsx-bestand. Dit bestand is tevens [via deze link](https://drive.google.com/uc?export=download&id=0B-DYcTNHFnIXUHVCTnc3SmhZSjA) te downloaden.


\blandscape

\scriptsize

```{r TabelEindgebruik, results='asis', echo=FALSE, message=FALSE}
tabel1 <- read.xlsx2("../Tabellen/Eindgebruik van resultaten.xlsx",sheetIndex=1,check.names = FALSE, colIndex = 1:7)
pandoc.table(
  tabel1,
  keep.line.breaks = TRUE,
  caption = "(\\#tab:eindgebruik)_Inschatting per eindgebruiker van het gebruik van de resultaten uit de MNM en van de gewenste vorm en frequentie van die resultaten._",
  split.tables = Inf,
  split.cells = c(10,5,20,20,10,20,10),
  justify = "lclllll"
)
```

\normalsize

\elandscape



De **eindgebruikers met prioriteit 1** zijn het Vlaamse natuurbeleid, het departement en de minister waaronder het  ressorteert en de middenveldsectoren. Zij staan in verband met de verschillende beweegredenen voor de MNM (hoofdstuk \@ref(beweegredenen)).
De Europese Commissie heeft de op één na hoogste prioriteit gekregen: haar wensen in verband met de rapportage voor de Habitatrichtlijn vormen geen primaire beweegreden voor de MNM 
(zie paragraaf \@ref(regelgeving)).
Wél zijn haar rapportagenoden een lidstaatverplichting én kunnen de antwoorden op de Vlaamse vragen voor de MNM worden omgezet naar antwoorden op de Europese milieuvragen aangaande de Habitatrichtlijn.

Omdat *ANB* de trekker en initiatiefnemer is van het ontwerp van de MNM, dekken typisch de wensen van ANB het volledige spectrum van de kernvragen. De twee in de tabel éérst vernoemde gebruiken van de MNM, met overeenkomstige motivatie, zijn zonder meer de meest belangrijke voor de planning van het (bron- en effectgerichte) actieve natuurbeleid door ANB. Om de praktische inzetbaarheid voor ANB te maximaliseren, moet in de rapportage dus heel vlot (op de voorgrond) de informatie beschikbaar zijn:

- die de **beleidsprioritering toelaat van aan te pakken milieuknelpunten per habitattype of regionaal belangrijk biotoop**, met daarbij de omvang van de problemen (diagnose op basis van toestand en trend);
- die **aangeeft of het milieugerichte natuurbeleid voor of achter zit op schema**, en hoe groot de voorsprong of achterstand dan is (voortgangsbewaking op basis van beoordeling).

In dit verband dient ook te worden nagegaan welke **indicatoren** kunnen worden ontwikkeld die deze informatie goed weergeven. Dit gebeurt best zowel milieudrukspecifiek als meer overkoepelend voor het natuurlijk milieu.

We maken verder de nuance dat de volledigheid en betrouwbaarheid van de detectie en prioritering van milieuknelpunten zal afhangen van de kwaliteit en volledigheid van het *abiotisch toetsingskader* (paragraaf \@ref(toetsingskader)).

Omdat de natuurbeleidscyclus per zes jaar vorm wordt gegeven, zijn de rechtstreeks natuurbeleidsmatig of politiek betrokkenen (prioriteit 1 en 2) het meest gebaat met een *rapportagecyclus* die deze **frequentie** volgt. Deze cyclus spoort samen tussen Vlaanderen en Europa: de laatste rapportage gebeurde in 2013, in verband met de toestand van 2007 tot en met 2012. In bepaalde gevallen is er vooral nood aan synthese en overzicht, in andere gevallen moet er meer achtergrondinformatie beschikbaar zijn. Specifiek voor ANB is het, in het kader van continue evaluatie van het actieve beleid, bijkomend nuttig om tussentijdse of continue (geautomatiseerde) updates van rapportage of online ontsluiting te kunnen raadplegen. De eindgebruikers met lagere prioriteit volgen minder of niet dit zesjaarlijkse ritme. Om aan hun - soms acute - verwachtingen te kunnen tegemoetkomen, is een *continu beschikbaar*, online rapportage-instrument optimaal, dat in de mate van het mogelijke up-to-date is.

Samengevat zijn dit de **rapportagevormen** die nader kunnen worden geïmplementeerd, en waartussen in latere ontwerpfasen (cf. hoofdstuk \@ref(vervolg)) afwegingen kunnen gebeuren:

- vanaf eindgebruikers met prioriteit 1:
    - rapport met resultaten en interpretaties;
    - syntheserapport diagnose (betrekt diagnose actueel en voor de toekomst);
    - syntheserapport voortgang (betrekt beoordeling actueel en voor de toekomst);
    - optioneel: website met up-to-date resultaten en indicatoren;
- voor de eindgebruiker met prioriteit 2:
    - databank en formulieren volgens de specificaties van het Europese Natura 2000 referentieportaal ^[Dit 
    gebeurt geïntegreerd met de resultaten uit monitoringsinitiatieven voor natuur, zie paragraaf \@ref(aanbod). De rapportage aan Europa gebeurt op het niveau van elk habitattype.];
- vanaf eindgebruikers met prioriteit 3:
    - website met up-to-date resultaten en indicatoren;
    - website waar data geraadpleegd en gedownload kunnen worden;
    - idealiter: live koppeling of integratie tussen milieudatabanken.

Behalve rapportage is ook een professionele, **doelgroepgerichte communicatie** essentieel, zowel over de implementatie van een meetnet als over de rapportagefasen: nieuwsbrieven, presentaties, persberichtgeving, enzovoort. Ook dit aspect dient in het kader van latere ontwerpfasen de nodige gestalte te krijgen. Voor meer informatie hierover verwijzen we naar @wouters:2008ontwerp1.


